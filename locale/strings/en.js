export default {
  appTitle: 'Just count it!',

  createCounterLabel: 'Create new counter',
  updateCounterLabel: 'Update counter',
  defaultValueLabel: 'Default value',
  namePlaceHolder: 'Name',
  createCounterButton: 'Add counter',
  updateCounterButton: 'Edit counter',
  emptyText: 'There is no counter to display.'
};
