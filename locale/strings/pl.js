export default {
  appTitle: 'Licznik',

  createCounterLabel: 'Stwórz licznk',
  updateCounterLabel: 'Zmień licznik',
  defaultValueLabel: 'Wartość domyślna',
  namePlaceHolder: 'Nazwa',
  createCounterButton: 'Dodaj licznik',
  updateCounterButton: 'Edytuj licznik',
  emptyText: 'Brak liczników do wyświetlenia'
};
