import I18n from 'react-native-i18n';
import en from './strings/en';
import pl from './strings/pl';

I18n.fallbacks = true;

I18n.translations = {
  en,
  pl,
};

export default I18n;
