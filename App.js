/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';

import {
  StyleSheet,
  Text,
  View,
  StatusBar,
  ToolbarAndroid,
  TouchableHighlight,
  ListView,
  AsyncStorage,
  Modal,
  ScrollView,
  Dimensions,
  TouchableOpacity,
} from 'react-native';

import AddCounterComponent from './components/addCounter/AddCounterComponent'
import CountersComponent from './components/countersList/CountersComponent'
import ModifierModalComponent from './components/modifier/ModifierModalComponent'

import {
  primaryColor, primaryDarkColor, accentColor,
  accentRippleColor
} from './styles/common/colors.js';

import Icon from 'react-native-vector-icons/MaterialIcons';

import I18n from './locale/i18n.js';

var counters = []

export default class App extends Component<{}> {

  constructor() {
    super()

    this.state = {
      counterDataSource: counters,
      shouldDisplayAdd: false,
      editingCounter: false,
      editCounterValue: 0,
      editCounterName: "",
      addModalVisible: false,
      subModalVisible: false,
      idOfModifingCounterValue: null,
    }
  }

  componentDidMount() {
    AsyncStorage.getItem('counters').then((data) => {
      var countersFromStorage = JSON.parse(data)

      if (countersFromStorage == null) {
        return
      }

      counters = countersFromStorage

      this.setState({
        counterDataSource: counters,
      })
    }).done();
  }

  counterAdded(counter) {

    if (this.state.editingCounter) {
      this.counterEdited(counter)

      return
    }

    var counterDataSource = counters
    counterDataSource.push(counter)

    this.setState({
      shouldDisplayAdd: false,
      counterDataSource: counterDataSource,
    });

    this.saveData();
  }

  counterEdited(counter) {
    var counterDataSource = counters
    counterDataSource[this.state.counterToEditRowId] = counter

    this.setState({
      shouldDisplayAdd: false,
      counterDataSource: counterDataSource,
      editingCounter: false
    });

    this.saveData();
  }

  saveData() {
    AsyncStorage.setItem('counters', JSON.stringify(counters))
  }

  counterDeleted(counterRowId) {
    var counterDataSource = counters
    counterDataSource.splice(counterRowId, 1)

    this.setState({
      shouldDisplayAdd: false,
      counterDataSource: counterDataSource,
      editingCounter: false,
      idOfModifingCounterValue: null,
    })

    this.saveData()
  }

  editCounter(counterRowId) {
    var counter = counters[counterRowId]

    this.setState({
      editingCounter: true,
      editCounterName: counter.name,
      editCounterValue: counter.value,
      shouldDisplayAdd: true,
      counterToEditRowId: counterRowId
    })
  }

  decrementCounter(counterRowId) {
    var counter = counters[counterRowId]
    counter.value -= 1

    var counterDataSource = counters
    counterDataSource[counterRowId] = counter

    this.setState({ counterDataSource: counters })
    this.saveData()
  }

  incrementCounter(counterRowId) {
    var counter = counters[counterRowId]
    counter.value += 1

    var counterDataSource = counters
    counterDataSource[counterRowId] = counter

    this.setState({ counterDataSource: counters })
    this.saveData()
  }

  subtractCounter(counterRowId) {
    this.setState({
      subModalVisible: true,
      idOfModifingCounterValue: counterRowId,
    })
  }

  addCounter(counterRowId) {
    this.setState({
      addModalVisible: true,
      idOfModifingCounterValue: counterRowId,
    })
  }

  fabAddClicked() {
    let isDisplayed = this.state.shouldDisplayAdd

    if (isDisplayed) {
      this.setState({
        editingCounter: false,
        shouldDisplayAdd: !isDisplayed
      })

      return
    }

    this.setState({shouldDisplayAdd: !isDisplayed})
  }

  valueSelected(value) {
    var counter = counters[this.state.idOfModifingCounterValue]
    counter.value += value

    var counterDataSource = counters
    counterDataSource[this.state.idOfModifingCounterValue] = counter

    this.setState({
      counterDataSource: counters,
      addModalVisible: false,
      subModalVisible: false,
    });

    this.saveData()
  }

  renderModal() {
    if (counters === undefined || counters === null || counters.length === 0) {
      return null
    }

    let title = this.state.addModalVisible ? 'Adding to ' : 'Substracting from '
    if (counters !== undefined && this.state.idOfModifingCounterValue !== null) {
      title = title.concat(counters[this.state.idOfModifingCounterValue].name)
    }
    let button = this.state.addModalVisible ? 'ADD' : 'SUB'

    if(this.state.addModalVisible || this.state.subModalVisible) {
      return(
        <ModifierModalComponent
          title = {title}
          okButtonText = {button}
          startValue = {2}
          endValue = {50}
          valueSelected = {(value) => {

            if (value === undefined) {
              this.setState({
                addModalVisible: false,
                subModalVisible: false,
              });

              return
            }
            if(this.state.subModalVisible) {
              this.valueSelected(value * (-1))

              return
            }
            this.valueSelected(value)
          }}
        />
      );
    }

    return null;
  }

  render() {

    return (
      <View style={styles.container}>
        {this.renderModal()}
        <StatusBar backgroundColor = {primaryDarkColor}/>
        <ToolbarAndroid title = {I18n.t('appTitle')} style = {styles.toolbar} titleColor = "white"/>

        <AddCounterComponent
          shouldDisplay = {this.state.shouldDisplayAdd}
          counterAdded = {this.counterAdded.bind(this)}
          editing = {this.state.editingCounter}
          value = {this.state.editCounterValue}
          name = {this.state.editCounterName}/>

        <CountersComponent
          counters = {this.state.counterDataSource}
          counterDeleted = {this.counterDeleted.bind(this)}
          editCounter = {this.editCounter.bind(this)}
          incrementCounter = {this.incrementCounter.bind(this)}
          decrementCounter = {this.decrementCounter.bind(this)}
          subtractCounter = {this.subtractCounter.bind(this)}
          addCounter = {this.addCounter.bind(this)}/>

        <TouchableHighlight
          style={styles.addButton}
          underlayColor={accentRippleColor}
          onPress={this.fabAddClicked.bind(this)}>

          <Icon name="add" size={30} color='white' />

        </TouchableHighlight>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  toolbar: {
    height: 56,
    backgroundColor: primaryColor,
  },
  addButton: {
    backgroundColor: accentColor,
    borderColor: accentColor,
    borderWidth: 1,
    height: 56,
    width: 56,
    borderRadius: 50,
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    bottom: 16,
    right:16,
    elevation: 2
  },
});
