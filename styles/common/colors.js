export let primaryColor = "#673AB7";
export let primaryDarkColor = "#512DA8";
export let accentColor = '#FFC107';

export let labelColor = '#B39DDB'

export let accentRippleColor = '#FFCA28';
export let rippleColor = '#EEEEEE'
