//Margins
export let baseMargin = 16;
export let smallMargin = 8;

//fonts sizes
export let normalFontSize = 24;
export let bigFontSize = 32;

//Elevations
export let cardViewElevation = 2;

//other
export let minimalTouchableSize = 48;
