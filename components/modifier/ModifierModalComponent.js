import React, { Component } from 'react'

import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Modal,
  ScrollView,
  Dimensions,
  TouchableOpacity,
} from 'react-native'

import {
  primaryColor,
} from '../../styles/common/colors.js';

const Screen = {
  width: Dimensions.get('window').width,
  height: Dimensions.get('window').height,
};

export default class ModifierModalComponent extends Component<{}> {

  constructor(props) {
    super(props)

    this.state = {
      chosenValue: props.chosenValue,
    }
  }

  choosedValue(value) {
    this.setState({
      chosenValue: value,
    })
  }

  getValues() {
    const values = []

    for (let i = this.props.startValue; i <= this.props.endValue; i++) {
      values.push(i)
    }

    return values
  }

  renderValue(value) {
    if (value === this.state.chosenValue) {
      return(
        <TouchableOpacity key={value} style={{ ...styles.valueContainer, backgroundColor: primaryColor, }}>
          <Text style={{ ...styles.value, color: '#fff', fontWeight: 'bold'}}>{value}</Text>
        </TouchableOpacity>
      );
    }

    return(
      <TouchableOpacity key={value} style={styles.valueContainer} onPress={() => {this.choosedValue(value)}}>
        <Text style={styles.value}>{value}</Text>
      </TouchableOpacity>
    );
  }

  render() {
    return(
      <Modal transparent onRequestClose={() => {this.props.valueSelected(undefined)}}>
        <View style={styles.modalBackground}>
          <View style={styles.modalContainer}>
            <Text style={styles.modalTitle}>{this.props.title}</Text>
            <ScrollView  contentContainerStyle={{flexDirection: 'row', flexWrap: 'wrap', padding: 4, justifyContent: 'space-around'}}>
            {
              this.getValues().map((i) => {
                let value = i + this.props.startValue;

                return this.renderValue(i + this.props.startValue)
              })
            }
            </ScrollView>
            <View style={{flexDirection: 'row', justifyContent: 'flex-end'}}>
              <TouchableOpacity onPress={() => this.props.valueSelected(undefined)}>
                <Text style={styles.buttonText}>CANCEL</Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => this.props.valueSelected(this.state.chosenValue)}>
                <Text style={styles.buttonText}>{this.props.okButtonText}</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
    )
  }
}

const styles = StyleSheet.create({
  modalBackground: {
    justifyContent: 'center',
    alignItems: 'center',
    width: Screen.width,
    height: Screen.height,
    flexDirection: 'row',
    padding: 16,
    backgroundColor: '#0005',
  },
  modalContainer: {
     height: 400,
     backgroundColor: '#fff',
     flex: 1,
     borderRadius: 4,
     overflow: 'hidden',
     elevation: 4,
  },
  modalTitle: {
    fontSize: 20,
    padding: 16,
    backgroundColor: primaryColor,
    color: '#fff',
  },
  buttonText: {
    fontSize: 16,
    padding: 15,
    color: primaryColor,
    fontWeight: 'bold',
  },
  valueContainer: {
    width: 48,
    height:48,
    borderColor: primaryColor,
    borderWidth: 2,
    margin: 4,
    alignItems: 'center',
    borderRadius: 25,
  },
  value: {
    fontSize: 22,
    padding: 8,
  }
});

AppRegistry.registerComponent('ModifierModalComponent', () => ModifierModalComponent);
