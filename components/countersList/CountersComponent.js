import React, { Component } from 'react';

import {
  AppRegistry,
  ListView,
  Image,
  Dimensions,
  View,
  StyleSheet,
  Text
} from 'react-native';

import I18n from './../../locale/i18n.js';

import CounterInteractableWrapperComponent from '../counter/CounterInteractableWrapperComponent'

const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
const screen = {
  width: Dimensions.get('window').width,
};

export default class CountersComponent extends Component<{}> {

  constructor(props) {
    super(props)

    this.state = {
      counterDataSource: ds.cloneWithRows(props.counters),
      counterDeleted: props.counterDeleted,
      editCounter: props.editCounter
    }
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      counterDataSource: ds.cloneWithRows(nextProps.counters)
    })
  }

  renderRow(counter, sectionId, rowId, highlightRow) {

    return(

      <CounterInteractableWrapperComponent
        name = {counter.name}
        initValue = {counter.value}
        counterRowId = {rowId}
        counterDeleted = {this.state.counterDeleted}
        editCounter = {this.state.editCounter}
        increment = {()=>{this.props.incrementCounter(rowId)}}
        decrement = {()=>{this.props.decrementCounter(rowId)}}
        subtract = {()=>{this.props.subtractCounter(rowId)}}
        add = {()=>{this.props.addCounter(rowId)}}/>
    )
  }

  render() {

    if (this.state.counterDataSource.getRowCount() == 0) {

      return (
        <View style={styles.container}>
           <Image
            width={screen.width / 2}
            height={screen.width / 2}
            source={{uri: 'ic_launcher'}}
            style={styles.emptyImage} />
            <Text>{I18n.t('emptyText')}</Text>
        </View>
      )
    }

    return (
      <ListView
        dataSource = {this.state.counterDataSource}
        renderRow = {this.renderRow.bind(this)}/>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
    borderWidth: 1,
    flexDirection: 'column',
  },

  emptyImage : {
    width: screen.width / 2,
    height: screen.width / 2,
    opacity: 0.4
  }
})

AppRegistry.registerComponent('CountersComponent', () => CountersComponent);
