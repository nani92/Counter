import React, { Component } from 'react';

import {
  AppRegistry, View, StyleSheet, Text,
  TextInput, Button,
  ToastAndroid
} from 'react-native';

import Icon from 'react-native-vector-icons/MaterialIcons';

import {
  baseMargin, smallMargin,
  cardViewElevation,
  bigFontSize
} from './../../styles/common/dimens.js';

import {
  primaryDarkColor, accentColor, labelColor
} from './../../styles/common/colors.js';

import I18n from './../../locale/i18n.js';

export default class AddCounterComponent extends Component<{}> {

  constructor(props) {
    super(props)

    this.state = {
      value: '0',
      name: '',
      isValueWrong: false,
      cardLabel: I18n.t('createCounterLabel'),
      buttonTitle: I18n.t('createCounterButton')
    }
  }

  onAddCounterClicked() {
    this.props.counterAdded({name: this.state.name, value: parseInt(this.state.value)})
    this.clearForm()
  }

  onValueChanged(value) {

    if (Number.isInteger(+value)) {
      this.setState({
        value: value,
        isValueWrong: isNaN(parseInt(value))
      })
    }
  }

  clearForm() {
    this.setState({
      value: '0',
      name: '',
      isValueWrong: false,
      cardLabel: I18n.t('createCounterLabel'),
      buttonTitle: I18n.t('createCounterButton')
    })
  }

  componentWillReceiveProps(nextProps) {

    if (nextProps.editing) {

      this.setState({
          value: nextProps.value,
          name: nextProps.name,
          isValueWrong: false,
          buttonTitle: I18n.t('updateCounterButton'),
          cardLabel: I18n.t('updateCounterLabel')
      })
    } else {
      this.clearForm()
    }
  }

  render() {

    if (this.props.shouldDisplay == false) {
      return null;
    }

    return (
      <View style={styles.cardView}>

        <Text style={styles.label}>{this.state.cardLabel}</Text>

        <TextInput
          onChangeText={(text) => this.setState({name: text})}
          value={this.state.name}
          placeholder={I18n.t('namePlaceHolder')}/>

        <View style={styles.addCounterHorizontalContainer}>

          <View>
            <Text style={{color: labelColor}}>{I18n.t('defaultValueLabel')}</Text>

            <TextInput
              style={styles.addCounterValue}
              onChangeText={(value) => this.onValueChanged(value)}
              value={`${this.state.value}`}
              keyboardType = 'numeric'
              maxLength = {6}/>
          </View>

          <View style = {styles.buttonWrapper}>
            <Button
              title = {this.state.buttonTitle}
              color = {accentColor}
              onPress = {() => {this.onAddCounterClicked()}}
              disabled = {this.state.name.length == 0 || this.state.isValueWrong }/>
          </View>

        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({

  cardView: {
    elevation: cardViewElevation,
    backgroundColor: 'white',
    margin: baseMargin,
    padding: smallMargin,
  },

  label: {
    color: labelColor,
  },

  addCounterHorizontalContainer: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'stretch',
    marginTop: baseMargin,
  },

  addCounterValue: {
    fontSize: bigFontSize,
    color: primaryDarkColor,
  },

  buttonWrapper: {
    flex: 50,
    marginLeft: baseMargin,
    justifyContent: 'flex-end',
    minWidth: 100,
  }
});

AppRegistry.registerComponent('AddCounterComponent', () => AddCounterComponent);
