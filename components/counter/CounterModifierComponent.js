import React, { Component } from 'react';

import {
  AppRegistry,
  View,
  StyleSheet,
  TouchableHighlight,
  ToastAndroid,
} from 'react-native';

import Icon from 'react-native-vector-icons/MaterialIcons';

import { primaryColor, rippleColor } from './../../styles/common/colors.js';

import { minimalTouchableSize } from './../../styles/common/dimens.js';

export default class CounterModifierComponent extends Component<{}> {
  render() {
    return (
      <View style={{justifyContent: 'center'}}>
        <TouchableHighlight
            style={styles.button}
            onPress={this.props.onPress}
            onLongPress={this.props.onLongPress}
            underlayColor={rippleColor}>
              <Icon name={this.props.iconName} size={30} color={primaryColor} />
        </TouchableHighlight>
      </View>
    );
}
}
const styles = StyleSheet.create({
  button: {
    minWidth: minimalTouchableSize,
    height: minimalTouchableSize,
    justifyContent: 'center',
    borderRadius: 100,
    justifyContent:'center',
    alignItems: 'center',
  },
});

AppRegistry.registerComponent('CounterModifierComponent', () => CounterModifierComponent);
