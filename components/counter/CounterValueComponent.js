import React, { Component } from 'react';

import {
  AppRegistry,
  View,
  StyleSheet,
  Text
} from 'react-native';

import { primaryDarkColor } from './../../styles/common/colors.js';
import { bigFontSize } from './../../styles/common/dimens.js';

export default class CounterValueComponent extends Component<{}> {
  render() {
    return (
      <View style= {styles.counterValueContainer}>
        <Text style={styles.counterValue}>{this.props.value}</Text>
      </View>
    );
}
}
const styles = StyleSheet.create({
    counterValueContainer: {
      flex: 30,
      justifyContent:'center',
    },
    counterValue: {
      fontSize: bigFontSize,
      textAlign: 'right',
      color: primaryDarkColor,
    },
});

AppRegistry.registerComponent('CounterValueComponent', () => CounterValueComponent);
