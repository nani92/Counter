import React, { Component } from 'react';

import {
  AppRegistry,
  View,
  StyleSheet,
  Text
} from 'react-native';


import { normalFontSize } from './../../styles/common/dimens.js';

export default class CounterNameComponent extends Component<{}> {
  render() {
    return (
      <View style= {styles.counterNameContainer}>
        <Text style={styles.counterName}>{this.props.name}</Text>
      </View>
    );
}
}
const styles = StyleSheet.create({
  counterNameContainer: {
    flex: 70,
    justifyContent:'center',
  },
  counterName: {
    fontSize: normalFontSize
  },
});

AppRegistry.registerComponent('CounterNameComponent', () => CounterNameComponent);
