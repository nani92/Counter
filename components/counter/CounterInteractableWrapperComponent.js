import React, { Component } from 'react';

import {
  AppRegistry,
  Dimensions,
  View,
  StyleSheet,
} from 'react-native';

import CounterComponent from './CounterComponent'
import CounterModifierComponent from './CounterModifierComponent'

import Interactable from 'react-native-interactable';

import {
  baseMargin
} from './../../styles/common/dimens.js';

const Screen = {
  width: Dimensions.get('window').width,
};

export default class CounterInteractableWrapperComponent extends Component<{}> {

  onDrawerSnap() {

  }

  delete() {
    this.interactable.snapTo({index: 0});
    this.props.counterDeleted(this.props.counterRowId);
  }

  edit() {
    this.interactable.snapTo({index: 0});
    this.props.editCounter(this.props.counterRowId);
  }

  render() {

    return (
      <View style = {{ display: 'flex'}}>

        <View style = {styles.modifiersWrapperView}>

          <CounterModifierComponent
            iconName='edit'
            onPress={()=>{this.edit()}}/>

          <CounterModifierComponent
            iconName='delete'
            onPress={()=>{this.delete()}}/>

        </View>


        <Interactable.View
          style= {{zIndex: 3}}
          horizontalOnly={true}
          snapPoints={[{x: 0}, {x: -Screen.width * 0.2}, {x:Screen.width * 0.2}]}
          onSnap={this.onDrawerSnap}
          ref = {(c) => this.interactable = c}>

          <CounterComponent
            name = {this.props.name}
            value = {this.props.initValue}
            increment = {this.props.increment}
            decrement = {this.props.decrement}
            subtract = {this.props.subtract}
            add = {this.props.add}/>

        </Interactable.View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  modifiersWrapperView: {
    width: Screen.width - baseMargin,
    position: 'absolute',
    zIndex: 2,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: baseMargin,
    paddingLeft: baseMargin,
    paddingRight: baseMargin
  }

})

AppRegistry.registerComponent('CounterInteractableWrapperComponent', () => CounterInteractableWrapperComponent);
