import React, { Component } from 'react';

import {
  AppRegistry,
  View,
  StyleSheet,
} from 'react-native';

import Icon from 'react-native-vector-icons/MaterialIcons';

import {
  baseMargin, smallMargin,
  cardViewElevation
} from './../../styles/common/dimens.js';

import CounterModifierComponent from './CounterModifierComponent'
import CounterNameComponent from './CounterNameComponent'
import CounterValueComponent from './CounterValueComponent'

export default class CounterComponent extends Component<{}> {

  constructor(props) {
    super(props)

    this.state = {value: this.props.value}
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      value: nextProps.value
    })
  }

  render() {
    return (

        <View style={styles.cardView}>

          <CounterModifierComponent
            iconName='remove'
            onPress={()=>{this.props.decrement()}}
            onLongPress={()=>this.props.subtract()}/>

          <View style= {styles.counterValuesContainer}>
            <CounterNameComponent name = {this.props.name}/>
            <CounterValueComponent value = {this.state.value}/>
          </View>

          <CounterModifierComponent
            iconName='add'
            onPress={()=>{this.props.increment()}}
            onLongPress={()=>this.props.add()}/>
        </View>

    );
  }
}
const styles = StyleSheet.create({
  cardView: {
    elevation: cardViewElevation,
    backgroundColor: 'white',
    margin: baseMargin,
    padding: smallMargin,
    flexDirection: 'row',
    zIndex: 3,
  },
  counterValuesContainer: {
    flex: 1,
    justifyContent:'center',
    flexDirection:'row',
    marginLeft: smallMargin,
    marginRight: smallMargin,
  },
});

AppRegistry.registerComponent('CounterComponent', () => CounterComponent);
